FROM continuumio/anaconda3:latest

RUN apt-get -y -q install vim-tiny

WORKDIR /app

COPY . /app

EXPOSE 8888

# This is how you to install additional python packages using pip (can also use conda too)
RUN pip --no-cache-dir install gensim

CMD ["jupyter", "notebook", "--ip='*'", "--port=8888", "--no-browser", "--allow-root"]
